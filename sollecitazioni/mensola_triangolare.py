# -*- coding: utf-8 -*-
"""
Created on Fri Feb 12 17:15:36 2021

@author: Vittorio Muzio
"""
#il programma calcola la funzione degli spostamenti di una mensola con carico triangolare tramite analogia di Mohr

import numpy as np
import scipy as sp
from scipy import integrate
import matplotlib.pyplot as plt

#dati
#geometrici
c_rr = 126 #[mm] corda reale alla radice
c_rr = c_rr/1000 #[m]
c_ar = 100 #[mm]corda adimensionale alla radice
c_ar = c_ar/1000 #[m]
F = c_rr/c_ar #fattore di scala tra profilo alla radice reale e adimensionale di gh
factor = 0.373 #fattore di scala tra profilo alla radice e profilo all'estremita' alare
s = 520 #[mm] span reale della semiala
s = s/1000 #[m]
# th = 6 #[mm]spessore profilo adimensionale
th = th/1000 #[m]
#momenti d'inerzia
#I_ar = I2
# # Ix = 5392.81905 #[mm4]
# Ix = 13845.3193 #[mm4]
# Ix = Ix/10**12 #[m4]
# # Iy = 46176.0518 #[mm4]
# Iy = 468996.12 #[mm4]
# Iy = Iy/10**12 #[m4]
# Ixy = 64241.8588 #[mm4]
# Ixy = Ixy/10**12 #[m4]
#forze
m = 120; #masssa totale di progetto, comprende: barca, velista, maggiorazione di sicurezza
L = m*9.81; #portanza totale di progetto in N
L_hw = L/2
Lm_tip = 2*L_hw/s #carico alla radice
#materiale
E = 121 #[GPa]
E = E*10**9 #[Pa]
#sigma_max = #[MPa]
#sigma_max = sigma_max*20**g #[Pa]

#calcolo del momento d'inerzia rispetto all'asse principale del profilo adimensionalizzato
# I1 = 0.5*(Ix+Iy+np.sqrt((Ix-Iy)**2+4*Ixy**2))
# I2 = 0.5*(Ix+Iy-np.sqrt((Ix-Iy)**2+4*Ixy**2))
I_ar = min(I1,I2)/1e12
# I_ar = 4006/10**12
# I_ar = 4000/10**12

#vettore span
x = np.linspace(0,s,1000)

#fattore di scala c_reale/c_ar in funzione della distanza dall'estremità alare
f = F*factor+(F-F*factor)/s*x
#inerzia della parte resistene del profilo reale in funzione della distanza dall'estremità alare
I_r = I_ar*f**4

#sollecitazione
c = Lm_tip/s*x
T = 0.5*Lm_tip/s*x**2
# Ta = sp.integrate.trapz(c,x)
# difT = T-Ta
M = Lm_tip/(6*s)*x**3
# Ma = sp.integrate.trapz(T,x)
# difM = M-Ma

# fig=plt.figure()
# ax = fig.add_subplot(111)
plt.plot(x,T)

#Mohr
#carico fittizio
q = M/I_r/E
Tf = np.zeros(1000)
Mf = np.zeros(1000)
for i in range(1000):
    Tf[i] = sp.integrate.trapz(x[0:i],q[0:i])
for i in range(1000):
    Mf[i] = sp.integrate.trapz(x[0:i],Tf[0:i])

# Mfa = sp.integrate.trapz(x,Tf)

freccia = Mf[999] #[m]

#massima sollecitazione radice
Lmax_hw = 0.5*0.5*1025*(17**2)*(0.5*s*c_rr*(1+factor))*1.6*0.8; #portanza massima, caso di AoA grande a V sostenuta

Lm_tip = 2*Lmax_hw/s #carico alla radice
Mmax = (Lm_tip*s**2)/6
sforzo_massimo = Mmax/I_r[999]*th/10**6 #[MPa] sforzo massimo approssimato su dorso o ventre del profilo







































