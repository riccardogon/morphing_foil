import pandas as pd
import os
import numpy as np
from scipy import interpolate


def reconstruct_foil(foil_pts):
    foil_up, foil_low = divide_foil(foil_pts)
    foil_up.sort_values(by=['x'], ascending=False, inplace=True)

    foil = foil_up.append(foil_low, ignore_index=True)
    foil.drop_duplicates(subset='x', keep='first', inplace=True)

    return foil


def divide_foil(foil_pts):
    pts_sorted = foil_pts.sort_values(by=['x'])
    pts_sorted.drop_duplicates(subset='x', inplace=True)
    pts_sorted.reset_index(drop=True, inplace=True)
    x_upper = []
    y_upper = []
    x_lower = []
    y_lower = []

    pts_len = len(pts_sorted)
    for i, pt in enumerate(pts_sorted['y']):
        if i == 0:
            x_upper.append(pts_sorted['x'][i])
            y_upper.append(pt)
            x_lower.append(pts_sorted['x'][i])
            y_lower.append(pt)
        elif i > (pts_len-20):
            dx_up = x_upper[-1] - x_upper[-2]
            dx_low = x_lower[-1] - x_lower[-2]
            dy_up = y_upper[-1] - y_upper[-2]
            dy_low = y_lower[-1] - y_lower[-2]
            dist_up = (abs(dx_up * (y_upper[-2] - pt) - dy_up * (x_upper[-2] - pts_sorted['x'][i])))/(np.sqrt(dx_up**2 + dy_up**2))
            dist_low = (abs(dx_low * (y_lower[-2] - pt) - dy_low * (x_lower[-2] - pts_sorted['x'][i])))/(np.sqrt(dx_low**2 + dy_low**2))
            if dist_up < dist_low:
                y_upper.append(pt)
                x_upper.append(pts_sorted['x'][i])
            else:
                x_lower.append(pts_sorted['x'][i])
                y_lower.append(pt)
        else:
            if pt >= ((y_upper[-1]+y_lower[-1])/2):
                y_upper.append(pt)
                x_upper.append(pts_sorted['x'][i])
            elif pt < ((y_upper[-1]+y_lower[-1])/2):
                x_lower.append(pts_sorted['x'][i])
                y_lower.append(pt)
            else:
                print('Error on point {}'.format(pt))

    Upper = pd.DataFrame(list(zip(x_upper, y_upper)), columns=['x', 'y'])
    Lower = pd.DataFrame(list(zip(x_lower[1:], y_lower[1:])), columns=['x', 'y'])   # Remove duplicate first point

    return Upper, Lower


# Constants
n_pts = 160
under_relaxation = 0.5
min_defl = 0.2  # mm

# For testing purposes on Pycharm
if os.getcwd() != 'C:\\morphing_foil_codes\\morphing_foil\\Xfoil':
    pass
else:
    import shutil
    os.chdir('..\\Testing')
    shutil.rmtree('.\\FoilReader_test')
    os.mkdir('.\\FoilReader_test')
    shutil.copy('.\\Out_x.dat', '.\\FoilReader_test\\Out_x.dat')
    shutil.copy('.\\Out_y.dat', '.\\FoilReader_test\\Out_y.dat')
    shutil.copy('.\\Out_z.dat', '.\\FoilReader_test\\Out_z.dat')
    shutil.copy('FoilXfoil.txt', '.\\FoilReader_test\\FoilXfoil.txt')
    os.chdir('.\\FoilReader_test')
    x_rigid = 40

# Creating the DataFrame containing coordinates, Index is Node ID
x_data = pd.read_csv('Out_x.dat', sep='\s+', names=['x'], skiprows=1)
y_data = pd.read_csv('Out_y.dat', sep='\s+', names=['y'], skiprows=1)
z_data = pd.read_csv('Out_z.dat', sep='\s+', names=['z'], skiprows=1)
data = x_data.copy()
data['y'] = y_data.copy()
# Reading the Xfoil file for rigid part 
foil_xf = pd.read_csv('FoilXfoil.txt', sep='\s+', names=['x', 'y'], skiprows=1)
data = data.append(foil_xf[foil_xf.x < x_rigid], ignore_index=True)

#%% Ordering the points for Xfoil through interpolation
foil_compl = reconstruct_foil(data)
tck, u = interpolate.splprep([foil_compl.x, foil_compl.y], s=0)

x = 0.5 * (1 + np.tan(np.linspace(-np.pi/4, np.pi/4, n_pts+1)))
new_points = interpolate.splev(x, tck)

# Save the foil
output_pts = pd.DataFrame(list(zip(new_points[0], new_points[1])), columns=['x', 'y'])
output_pts.to_csv('FoilOutput.txt', index=None, sep=' ', header=['Foil_output', ''])

# Xfoil blending commands
xfoilInput = 'xfoil_input_blending.txt'
with open(xfoilInput, "w") as fid:
    fid.write("INTE\n")
    fid.write("F \n")
    fid.write('FoilXfoil.txt\n')
    fid.write("F \n")
    fid.write('FoilOutput.txt\n')
    fid.write(str(under_relaxation) + '\n')
    fid.write('Foil_blended \n')
    fid.write('PCOP\n')
    fid.write('SAVE FoilInput.txt\n')

# Run the XFoil calling command
os.system("xfoil.exe < xfoil_input_blending.txt")

# Convergence
# Obtaining the blended foil and output x and y arrays
blend = pd.read_csv('FoilInput.txt', sep='\s+', names=['x', 'y'], skiprows=1)
X = blend.x
Y = blend.y

# Dividing in upper and lower for interpolation
xf_up, xf_low = divide_foil(foil_xf)
bl_up, bl_low = divide_foil(output_pts)
# Retaining only the trailing edge aft of the rigid part
xf_up = xf_up[xf_up.x > x_rigid]
xf_low = xf_low[xf_low.x > x_rigid]
bl_up = bl_up[bl_up.x > x_rigid]
bl_low = bl_low[bl_low.x > x_rigid]

y_xf_up = np.interp(bl_up.x, xf_up.x, xf_up.y)
y_xf_low = np.interp(bl_low.x, xf_low.x, xf_low.y)

defl_up = bl_up.y - y_xf_up
defl_low = bl_low.y - y_xf_low
defl = defl_up.append(defl_low)

if abs(defl).max() < min_defl:
    convergence = 1
else:
    convergence = 0
