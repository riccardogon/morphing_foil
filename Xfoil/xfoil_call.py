import os
import numpy as np

#%% Run costants
# For testing purposes on Pycharm
if os.getcwd() != 'C:\\morphing_foil_codes\\morphing_foil\\Xfoil':
    vel = Vel
    AoA = str(AoA)
else:
    import shutil
    vel = 10
    AoA = '0'
    iteration = 0
    x_rigid = 70
    foil_head = 0.4           # [m]
    os.chdir('..\\Testing')
    # dataBuffer = np.genfromtxt('FoilInput.txt', skip_header=1, delimiter=[14, 12])
    dataBuffer = np.loadtxt('FoilInput.txt', skiprows=1, delimiter=',')
    X = dataBuffer[:, 0]
    Y = dataBuffer[:, 1]
    shutil.rmtree('.\\xfoil_call_test')
    os.mkdir('.\\xfoil_call_test')
    os.chdir('.\\xfoil_call_test')

kin_visc = 0.0000011892     # ITTC for 15°C
rho = 1026.0210             # ITTC for 15°C
p_vap = 2291.4 - 101325     # ITTC for 20°C
g = 9.807
foilLength = 0.100          # [m]
refPres = rho * g * foil_head
Re = vel * foilLength / kin_visc
z_section = 0.15  # Z at which apply the pressure in the mechanical analysis
numNodes = '200'
xf_iter = '200'
Ncrit = '3'

#%% IO names
foilFile = 'FoilInput.txt'
saveFlnmAF = 'FoilXfoil.txt'
saveFlnmCp = 'Cp.txt'
saveFlnmPC = 'P_complete.txt'
saveFlnmPUp = 'P_up.txt'
saveFlnmPDown = 'P_down.txt'
saveFlnmRes = 'Polar.dat'
xfoilFlnm = 'xfoil_input.txt'

# Create foil input
X_0 = np.array(X)
Y_0 = np.array(Y)
np.savetxt(foilFile, X=np.transpose([X_0, Y_0]), delimiter=',', fmt='%.4f', header='FoilInput', comments=' ')

# Create the airfoil
fid = open(xfoilFlnm, "w")
fid.write("LOAD " + str(foilFile) + "\n\n")
fid.write("SAVE " + saveFlnmAF + "\n")
fid.write('GDES\n UNIT\n')
fid.write("X\n GSET\n \n")
fid.write("PPAR\n")
fid.write("N " + numNodes + "\n")
fid.write("\n\n")
fid.write("OPER\n")
fid.write("ITER " + xf_iter + "\n")
fid.write("VISC\n")
fid.write(str(round(Re)) + "\n")
fid.write('VPAR\n')
fid.write('N\n' + Ncrit + '\n \n')
fid.write('PACC\n')
fid.write(saveFlnmRes + '\n \n')
fid.write("ALFA " + AoA + "\n")
fid.write("CPWR " + saveFlnmCp + "\n")
fid.close()

# Run the XFoil calling command
os.system("xfoil.exe < xfoil_input.txt")

# %% READ DATA FILE: PRESSURE COEFFICIENT

# Load the data from the text file
dataBuffer = np.genfromtxt(saveFlnmCp, delimiter=[10, 9, 9])

# Extract data from the loaded dataBuffer array
X_0 = dataBuffer[2:, 0] * foilLength * 1000
Y_0 = dataBuffer[2:, 1] * foilLength * 1000
Cp_0 = dataBuffer[2:, 2]

# %% Retrieve Pressure from pressure coefficient

P = Cp_0 * 0.5 * rho * vel**2 + refPres
cavitation = 0
if P.min() < p_vap:
    cavitation = 1
Z_0 = np.ones_like(X_0) * z_section

P_mat = np.transpose([X_0, Y_0, Z_0, P])
np.savetxt(saveFlnmPUp, X=P_mat[:int(len(P)/2)], delimiter=',', fmt='%.4f', header='X, Y, Z, P')
np.savetxt(saveFlnmPDown, X=P_mat[int(len(P)/2):], delimiter=',', fmt='%.4f', header='X, Y, Z, P')

# P_mat_out = P_mat[P_mat[:, 0] > (x_rigid-1)]
np.savetxt(saveFlnmPC, X=P_mat, delimiter=',', fmt='%.4f', header='X, Y, Z, P')

iteration = iteration + 1
