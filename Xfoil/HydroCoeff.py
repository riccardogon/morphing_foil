import pandas as pd
import numpy as np
import os


def xfoil_run(foilFile, angle, ReNum, ObjNC, numNodes=400, xfIter=300):
    xfoilFlnm = 'xfoil_input.txt'

    # Loop for Ncrit
    steps = [0, -0.05, 0.05, -0.1, 0.1, -0.15, 0.15]
    result = False
    iteration = 0
    while not result:
        Ncrit = ObjNC + float(steps[iteration])
        polarFlnm = 'Polar_{}.dat'.format(Ncrit) 

        # Create the xfoil command lines
        fid = open(xfoilFlnm, "w")
        fid.write("LOAD " + str(foilFile) + "\n\n")
        fid.write('GDES\nUNIT\n')
        fid.write("X\nGSET\n \n")
        fid.write("PPAR\n")
        fid.write("N " + numNodes + "\n")
        fid.write("\n\n")
        fid.write("OPER\n")
        fid.write("ITER " + xfIter + "\n")
        fid.write("VISC\n")
        fid.write(str(round(ReNum)) + "\n")
        fid.write('VPAR\n')
        fid.write('N\n' + str(Ncrit) + '\n \n')
        fid.write('PACC\n')
        fid.write(polarFlnm + '\n \n')
        fid.write("ALFA " + angle + "\n")
        fid.close()

        # Run the XFoil calling command
        os.system("xfoil.exe < xfoil_input.txt")
        
        # Check the results
        polar = np.genfromtxt(fname=polarFlnm, delimiter=[8, 9, 10, 10, 9, 9, 9])
        if polar.shape[0] != 13:
            iteration += 1
        else:
            result = True

    liftCoeff = polar[12, 1]
    dragCoeff = polar[12, 2]
    return liftCoeff, dragCoeff


if os.getcwd() != 'C:\\morphing_foil_codes\\morphing_foil\\Xfoil':
    vel = Vel
    AoA = str(AoA)
else:
    import shutil
    vel = 2.2
    AoA = '4'
    # Changing directory
    os.chdir('..\\Testing')
    # dataBuffer = np.genfromtxt('FoilInput.txt', skip_header=1, delimiter=[14, 12])
    dataBuffer = np.loadtxt('FoilInput.txt', skiprows=1, delimiter=',')
    X = dataBuffer[:, 0]
    Y = dataBuffer[:, 1]
    shutil.rmtree('.\\HydroCoeff_test')
    os.mkdir('.\\HydroCoeff_test')
    os.chdir('.\\HydroCoeff_test')

# Constants for Reynolds Number and vapour pressure
kin_visc = 0.0000011892     # ITTC for 15°C
rho = 1026.0210             # ITTC for 15°C
p_vap = 2291.4 - 101325     # ITTC for 20°C
g = 9.807
foilLength = 0.100          # [m]
# refPres = rho * g * foil_head TODO: add check for cavitation
Re = vel * foilLength / kin_visc

# Xfoil constants
meshNodes = '300'
maxIter = '200'
NcritObjective = 3

# Create foil input
foilFlnm = 'FoilInput.txt'
X_0 = np.array(X)
Y_0 = np.array(Y)
np.savetxt(foilFlnm, X=np.transpose([X_0, Y_0]), delimiter=',', fmt='%.4f', header='FoilInput', comments=' ')

cl, cd = xfoil_run(foilFile=foilFlnm, angle=AoA, ReNum=Re, ObjNC=NcritObjective, numNodes=meshNodes, xfIter=maxIter)
