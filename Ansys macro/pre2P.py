﻿# encoding: utf-8
# 2021 R1

# Change the CAD file
system = GetSystem(Name="SYS 1")
geometry1 = system.GetContainer(ComponentName="Geometry")
geometry1.SetFile(FilePath=".\\foil2d.3dm")
geometryComponent1 = system.GetComponent(Name="Geometry")
geometryComponent1.Update(AllDependencies=True)

# Change the Pressure file
SetScriptVersion(Version="21.1.216")
system1 = GetSystem(Name="SYS")
setup1 = system1.GetContainer(ComponentName="Setup")
externalLoadData1 = setup1.GetExternalLoadData()
externalLoadFileData1 = externalLoadData1.GetExternalLoadFileData(Name="ExternalLoadFileData")
externalLoadFileData1.ModifyFileData(FilePath=r".\\P_up.txt")
externalLoadFileData2 = externalLoadData1.GetExternalLoadFileData(Name="ExternalLoadFileData 1")
externalLoadFileData2.ModifyFileData(FilePath=r".\\P_down.txt")
