# encoding: utf-8
# 2021 R1

SetScriptVersion(Version="21.1.216")

import os

system1 = GetSystem(Name="SYS 1")

setup1 = system1.GetContainer(ComponentName="Solution")
setup1.Edit(Interactive=False)

wd = '"' + os.getcwd() + '"'

script_comm = """
import os

os.chdir({:}.replace(',', os.sep))

names = []
id = []

i = 0
for ns in DataModel.Project.Model.NamedSelections.Children:
    names.append(ns.Name)
    id.append(i)

# Retrieving the Surface_Foil NS
ind = names.index('Surface_Foil')
NS_SF = DataModel.Project.Model.NamedSelections.Children[ind] 

dy = DataModel.Project.Model.Analyses[0].Solution.AddUserDefinedResult()
dy.Expression = 'LOC_DEFY'
dy.Name = 'Deflection_y'
dy.ScopingMethod = GeometryDefineByType.Component
dy.Location = NS_SF

dx = DataModel.Project.Model.Analyses[0].Solution.AddUserDefinedResult()
dx.Expression = 'LOC_DEFX'
dx.Name = 'Deflection_x'
dx.ScopingMethod = GeometryDefineByType.Component
dx.Location = NS_SF

dz = DataModel.Project.Model.Analyses[0].Solution.AddUserDefinedResult()
dz.Expression = 'LOC_DEFZ'
dz.Name = 'Deflection_z'
dz.ScopingMethod = GeometryDefineByType.Component
dz.Location = NS_SF

dy.EvaluateAllResults()

dy.ExportToTextFile(False, 'Out_y.dat')
dx.ExportToTextFile(False, 'Out_x.dat')
dz.ExportToTextFile(False, 'Out_z.dat')
""".format(wd.replace('\\', ','))

setup1.SendCommand(Language='Python', Command=script_comm)

