# encoding: utf-8
# 2020 R1

SetScriptVersion(Version="20.1.164")

# Change the Pressure file
system1 = GetSystem(Name="SYS")
setup1 = system1.GetContainer(ComponentName="Setup")
externalLoadData1 = setup1.GetExternalLoadData()
externalLoadFileData1 = externalLoadData1.GetExternalLoadFileData(Name="ExternalLoadFileData")
externalLoadFileData1.ModifyFileData(FilePath=r".\\P.txt")
setupComponent1 = system1.GetComponent(Name="Setup")
setupComponent1.Update(AllDependencies=True)

# Change the CAD file
system = GetSystem(Name="SYS 1")
geometry1 = system.GetContainer(ComponentName="Geometry")
geometry1.SetFile(FilePath=".\\foil2d.3dm")
geometryComponent1 = system.GetComponent(Name="Geometry")
geometryComponent1.Update(AllDependencies=True)

# Pre macro
setup = system.GetContainer(ComponentName="Setup")
setup.Edit(Interactive=False)

script_comm = """
# Selection
SlMn = ExtAPI.SelectionManager
SlMn.ClearSelection()
Sel = SlMn.CreateSelectionInfo(SelectionTypeEnum.GeometryEntities)

#Model
model = DataModel.Project.Model

# Get parts
Parts = model.Geometry.GetChildren(DataModelObjectCategory.Part,True)

#print("Listing properties of Items:")
with Transaction():             # Suppress GUI update until finish to improve speed
    for Part in Parts:
        PName = Part.Name
        # print("Part name: ",PName)

        tmp_ls = []
        for Body in Part.Children:
            BName = Body.Name
            BId = Body.GetGeoBody().Id
            # print("Body name: "+BName+" ,BId: "+str(BId))
            tmp_ls.append(BId)
            #region Set object propertty


# Collect all body Ids in temporary list

        # ExtAPI.SelectionManager.NewSelection(Sel)
        Sel.Ids = tmp_ls
        #print("Sel.Ids: ",Sel.Ids)
        # Sel.Entities = [Body.GetGeoBody()]
        # Create named selection
        MyNS = model.AddNamedSelection()
        MyNS.Name = Part.Name
        MyNS.Location = Sel


NS_Surface_Foil = model.AddNamedSelection()
NS_Surface_Foil.Name = 'Surface_Foil'

Sel.Ids = [90, 36]
NS_Surface_Foil.Location = Sel

NS_FixedSup = model.AddNamedSelection()
NS_FixedSup.Name = 'Support'
Sel.Ids = [35, 58, 89]
NS_FixedSup.Location = Sel

Geometry = model.Geometry
for part in Geometry.Children:
    if part.Name == 'Lower':
        part.Material = 'lower from al'
    elif part.Name == 'Core':
        part.Material = 'core from PVC 60'
    elif part.Name == 'Upper':
        part.Material = 'upper from al'

StatStruct = model.Analyses[0]

ImportLoad = StatStruct.GetChildren(DataModelObjectCategory.ImportedLoad, True)[0]
ImportLoad.Location = NS_Surface_Foil

Fixed_Sup = StatStruct.AddFixedSupport()
Fixed_Sup.Location = NS_FixedSup

"""

setup.SendCommand(Language='Python', Command=script_comm)
