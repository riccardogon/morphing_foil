"""
Script for generating airfoil with Bezier curves using Bernstein polynomials
"""

import numpy as np
import matplotlib.pyplot as plt
import random


def cubic_bezier_1d(cont_pts, vec_t):
    Bez = []
    for t in vec_t:
        Bez.append((1-t)**3*cont_pts[0] + 3*(1-t)**2*t*cont_pts[1] + 3*(1 - t)*t**2*cont_pts[2] + t**3*cont_pts[3])
    return Bez


def cubic_bezier(xc, yc, n_pts, distr='', endpoint=True):
    if distr == 'cos':
        vec_t = (1 - np.cos(np.linspace(0, np.pi / 2, num=n_pts, endpoint=endpoint)))
    elif distr == 'sin':
        vec_t = (np.sin(np.linspace(0, np.pi / 2, num=n_pts, endpoint=endpoint)))
    else:  # Simply costant
        vec_t = np.linspace(0, 1, num=n_pts, endpoint=endpoint)
    xBez = cubic_bezier_1d(xc, vec_t)
    yBez = cubic_bezier_1d(yc, vec_t)
    return xBez, yBez


def half_simm_foil(xc, yc, n_pts):
    x = np.zeros([n_pts])
    y = np.zeros([n_pts])
    x[:n_pts//2], y[:n_pts//2] = cubic_bezier(xc[:4], yc[:4], n_pts=n_pts//2, distr='cos', endpoint=False)
    x[n_pts//2:], y[n_pts//2:] = cubic_bezier(xc[3:], yc[3:], n_pts=n_pts//2, distr='sin', endpoint=True)

    return x, y


def simm_foil(xc, yc, n_pts):
    xh, yh = half_simm_foil(xc, yc, n_pts)
    x = np.concatenate([xh[::-1], xh[1:]])
    y = np.concatenate([yh[::-1], -yh[1:]])
    return x, y


def camber(xc, yc, n_pts):
    x, y = cubic_bezier(xc, yc, n_pts=n_pts, endpoint=True)
    return x, y


def foil_construct(xc, yc, n_pts):
    # x = np.zeros([n_pts])
    # y = np.zeros([n_pts])
    # x[:n//2], Y_inp[:n//2] = cubic_bezier(x_pts[:4], n // 2)
    # X_inp[n//2:], Y_inp[n//2:] = cubic_bezier(x_pts[3:], n // 2)
    pass


# Costants
n = 100

# Controls for testing, to be deleted
th = 0.12
th_pos = 0.25
L = 1

# Control points
x2 = random.random() * th_pos * 0.9
x3 = th_pos
x4 = random.random() * (1 - th_pos) * 0.9 + th_pos + 0.1
x5 = random.random() * (1 - th_pos) + th_pos
y1 = random.random() * th/2
y2 = th/2
y5 = random.random() * th/2
y6 = 0.001

x_pts = [0, 0, x2, x3, x4, x5, 1]
y_pts = [0, y1, y2, y2, y2, y5, y6]

cx = [0, 0.25, 0.75, 1]
cy = [0, 0.05, 0.1, 0]

if __name__ == '__main__':
    X_inp, Y_inp = simm_foil(x_pts, y_pts, n)
    c = camber(cx, cy, n_pts=200)
    plt.plot(X_inp, Y_inp, 'r')
    # plt.plot(X_inp, -Y_inp, 'r')
    plt.plot(x_pts, y_pts, '+')
    # plt.plot(X_inp, Y_inp, 'k.')
    plt.plot(c[0], c[1])
    plt.axis('equal')
