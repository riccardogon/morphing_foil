import os
import numpy as np
import pandas as pd


def xfoilRun(foilFile, Re, Ncrit, flapX, flapY, flapAngle, foilAngRes, foilAngleLims, numNodes, xfIter, velocity):

    xfoilInpFname = 'xfInp.txt'
    svFnm = 'Polar_v{:}_f{:}.txt'.format(velocity, flapAngle)
    
    with open(xfoilInpFname, "w") as fid:
        fid.write("LOAD " + str(foilFile) + "\n\n")
        fid.write('GDES\nUNIT\nFLAP\n')
        fid.write('{:}\n'.format(flapX))
        fid.write('999\n{:}\n'.format(flapY))
        fid.write('{:}\n'.format(-flapAngle))
        fid.write("X\n GSET\n \n")
        fid.write("PPAR\n")
        fid.write("N " + str(numNodes) + "\n")
        fid.write("\n\n")
        fid.write("OPER\n")
        fid.write("ITER " + str(xfIter) + "\n")
        fid.write("VISC\n")
        fid.write(str(round(Re)) + "\n")
        fid.write('VPAR\n')
        fid.write('N\n' + str(Ncrit) + '\n \n')
        fid.write('PACC\n')
        fid.write(svFnm + '\n \n')
        fid.write("ALFA " + str(foilAngleLims[0]) + "\n")
        fid.write("ASEQ {:} {:} {:}".format(str(foilAngleLims[0]+foilAngRes), foilAngleLims[1], foilAngRes))

    os.system("xfoil.exe < xfInp.txt")


def flap_variation(foilFile, Re, Ncrit, flapX, flapY, flapAngleLims, flapAngleRes, foilAngleLims, foilAngRes, numNodes,
                   xfIter, velocity):
    flapAngles = np.arange(flapAngleLims[0], flapAngleLims[1]+1, flapAngleRes).tolist()

    for flAng in flapAngles:
        xfoilRun(foilFile=foilFile, Re=Re, Ncrit=Ncrit, flapX=flapX, flapY=flapY, flapAngle=flAng, foilAngRes=foilAngRes
                 , foilAngleLims=foilAngleLims, numNodes=numNodes, xfIter=xfIter, velocity=velocity)


def foil_flap_analysis(foilFile, charLenght, velocityLims=(1, 10), velocityRes=3, kinVisc=1.1892e-6, Ncrit=3, flapX=0.6,
                       flapY=0.5, flapAngleLims=(0, 0), flapAngleRes=5, foilAngleLims=(-10, 10), foilAngRes=1,
                       numNodes=200, xfIter=200):

    velocities = np.arange(velocityLims[0], velocityLims[1]+1, velocityRes)
    for velocity in velocities:
        Re = velocity * charLenght / kinVisc
        flap_variation(foilFile=foilFile, Re=Re, Ncrit=Ncrit, flapX=flapX, flapY=flapY, flapAngleLims=flapAngleLims,
                       flapAngleRes=flapAngleRes, foilAngleLims=foilAngleLims, foilAngRes=foilAngRes, numNodes=numNodes,
                       xfIter=xfIter, velocity=velocity)


if __name__ == '__main__':
    wdir = r'C:\Users\richi\Desktop\test_run\XfoilAnalysis'
    os.chdir(wdir)
    foil = 'C:\\Users\\richi\\Desktop\\test.txt'
    L = 0.1
    AoA = (-10, 10)
    angRes = 0.1
    xflap = 0.6
    yflap = 0.5
    flapLimits = [-40, 5]
    flapStep = 5
    nCrit = 3
    kVisc = 1.1892e-6
    vel = (1, 10)
    velResolution = 0.5

    foil_flap_analysis(foilFile=foil, charLenght=L, velocityLims=vel, velocityRes=velResolution, kinVisc=kVisc,
                       Ncrit=nCrit, flapX=xflap, flapY=yflap, flapAngleLims=flapLimits, flapAngleRes=flapStep,
                       foilAngleLims=AoA, foilAngRes=angRes, numNodes=300, xfIter=200)
