"""
Vpp implementation for evaluating the lift force of a passively morphing hydrofoil

Papers used:
(1)     Development and use of a Velocity Prediction Program to compare the effects of changes to foil arrangement
        on a hydro-foiling Moth dinghy - Findlay et al
(2)     Investigating sailing styles and boat set-up on the performance of a hydrofoiling Moth dinghy - Findlay et al
(3)     Evaluation of the Performance of a Hydro-Foiled Moth by Stability and Force Balance Criteria - Boegle
(4)     Flight Dynamics and Stability of a Hydrofoiling International Moth with a DVPP - Eggert
(5)     Experimental measurement and simplified prediction of T-foil performance for monohull dinghies - Day et al
(6)     Design of a foiling Optimist - Andersson et al
(7)     Full Scale Measurements on a Hydrofoil International Moth - Beaver et al
"""

import numpy as np
import os
from math import sin, cos, pi, log10, sqrt, copysign, radians
from scipy.interpolate import interp2d, interp1d
import matplotlib.pyplot as plt


def read_coeff_matrix(fnameCL, fnameCD, fnameCLred, path=None):
    """
    Reads the matrices for CL, CD and the reduction of CL due to the presence of the free surface. If path is provided
    the filenames are relative to the path.

    :param fnameCL: Filename or path of the Lift coefficient matrix (in .npy format)
    :type fnameCL: str
    :param fnameCD: Filename or path of the Drag coefficient matrix (in .npy format)
    :type fnameCD: str
    :param fnameCLred: Filename or path of the reduction of lift coefficient matrix (in .npy format)
    :type fnameCLred: str
    :param path: Absolute path from which the filenames are relative - Default: None
    :type path: str

    :return: Returns a tuple containing three numpy 2-dimensional arrays: (CL, CD, CLred).
        With:
        CL: Lift coefficient matrix
        CD: Drag coefficient matrix
        CLred: Lift reduction matrix
    :rtype: (numpy.ndarray, numpy.ndarray, numpy.ndarray)
    """
    if path is None:
        pass
    else:
        fnameCL = os.path.join(path, fnameCL)
        fnameCD = os.path.join(path, fnameCD)
        fnameCLred = os.path.join(path, fnameCLred)

    CL = np.load(fnameCL)
    CD = np.load(fnameCD)
    CLred = np.load(fnameCLred)
    return CL, CD, CLred


def sail(vApp, betaApp, SailSurf=8, SailAR=3.15, SailLenght=0.5, clSail=1.1, cdWindage=1.225, WindageSur=0.89, k57=1.05,
         kinViscAir=14.88e-6, rhoAir=1.225):
    """
    :param vApp: Apparent wind speed
    :param betaApp: Apparent wind angle
    :param SailSurf: Surface of the sail - Default by (2) table 1 page 10
    :param SailAR: Aspect ratio of the sail - Default by (3) figure C-28 page 94
    :param SailLenght: Lenght of the sail for Reynolds - Default by (3) figure C-28 page 94
    :param clSail: Lift coefficient of the sail - Default by (1) page 4
    :param cdWindage: Drag coefficient of windage - Default by (4) page 82 [CD_X_B]
    :param WindageSur: Projected area of the boat, hiking rack and sailor - Default by (4) page 82 [S_X_B]
    :param k57: Form factor for ITTC '57 - Default by (1) page 4
    :param kinViscAir: Kinematic viscosity of air
    :param rhoAir: Density of air
    :return: fx: Force on the x direction (driving force)
        fy: Force on the y direction (heel force)
    :rtype: (float, float)
    """
    # --- Lift ---
    # Formulation 11 on (1) [page 4]
    Lift = 0.5 * rhoAir * SailSurf * vApp**2 * clSail

    # --- Drag ---
    # Formulation 1 on (1) [page 3]
    DW = 0.5 * rhoAir * vApp**2 * WindageSur * cdWindage
    # Formulation 6, 7, 8 on (1) [page 4]
    cdI = 1/(pi * SailAR) * clSail**2
    # Reynolds number
    Re = vApp * SailLenght / kinViscAir
    # Formulation 4 on (1) [page 4] - ITTC '57
    cdF = 0.075 / (log10(Re) - 2)**2
    # Formulation 5 on (1) [page 4]
    cdP = 2 * cdF * (1 + k57)
    # Formulation 9 on (1) [page 4]
    cd = cdI + cdP
    # Formulation 10 on (1) [page 4]
    Drag = 0.5 * rhoAir * SailSurf * vApp**2 * cd + DW

    # Forces on boat axis
    # Formulation 12 on (1) [page 4]
    fy = Lift * sin(betaApp) - Drag * cos(betaApp)
    fx = Lift * cos(betaApp) + Drag * sin(betaApp)
    return fx, fy


def sail_reduction(vApp, betaApp, hydHead, SailSurf=8, SailAR=3.15, SailLenght=0.5, clSail=1.5, cdWindage=1.225,
                   WindageSur=0.89, k57=1.05, kinViscAir=14.88e-6, rhoAir=1.225, strutHeight=1, sailCenter=3.282,
                   weight=80, maxBeam=1.5, g=9.81, momConv=0.1):
    """
    :param vApp: Apparent wind speed
    :param betaApp: Apparent wind angle
    :param hydHead: Hydrostatic head over the foil
    :param SailSurf: Surface of the sail - Default by (2) table 1 page 10
    :param SailAR: Aspect ratio of the sail - Default by (3) figure C-28 page 94
    :param SailLenght: Lenght of the sail for Reynolds - Default by (3) figure C-28 page 94
    :param clSail: Lift coefficient of the sail - Default by (1) page 4
    :param cdWindage: Drag coefficient of windage - Default by (4) page 82 [CD_X_B]
    :param WindageSur: Projected area of the boat, hiking rack and sailor - Default by (4) page 82 [S_X_B]
    :param k57: Form factor for ITTC '57 - Default by (1) page 4
    :param kinViscAir: Kinematic viscosity of air
    :param rhoAir: Density of air
    :param strutHeight: Lenght of the strut from the main foil to the hull
    :param sailCenter: Vertical center of the sail
    :param weight: Weight of the sailor in Kg
    :param maxBeam: Maxim position outward of the sailor (CoG)
    :param g: Gravitational acceleration
    :param momConv: Residual moment for which the equilibrium is fulfilled

    :return fx: Force on the x direction (driving force)
    :return fy: Force on the y direction (heel force)
    """
    depFact = 1
    depFactDiff = 1
    while True:
        # --- Lift ---
        # Formulation 11 on (1) [page 4]
        Lift = 0.5 * rhoAir * SailSurf * vApp**2 * clSail * depFact

        # --- Drag ---
        # Formulation 1 on (1) [page 3]
        DW = 0.5 * rhoAir * vApp**2 * WindageSur * cdWindage
        # Formulation 6, 7, 8 on (1) [page 4]
        cdI = 1/(pi * SailAR) * clSail**2
        # Reynolds number
        Re = vApp * SailLenght / kinViscAir
        # Formulation 4 on (1) [page 4] - ITTC '57
        cdF = 0.075 / (log10(Re) - 2)**2
        # Formulation 5 on (1) [page 4]
        cdP = 2 * cdF * (1 + k57)
        # Formulation 9 on (1) [page 4]
        cd = cdI + cdP
        # Formulation 10 on (1) [page 4]
        Drag = 0.5 * rhoAir * SailSurf * vApp**2 * cd + DW

        # Forces on boat axis
        # Formulation 12 on (1) [page 4]
        fy = Lift * sin(betaApp) - Drag * cos(betaApp)
        fx = Lift * cos(betaApp) + Drag * sin(betaApp)

        M = fx * (strutHeight - hydHead/2 + sailCenter)
        if M < (weight * g * maxBeam):
            if depFact == 1:
                return fx, fy
            elif depFact < 1:
                depFactDiff = depFactDiff/2
                depFact = depFact + depFactDiff/2
        elif M > (weight * g * maxBeam):
            depFact = depFact - depFactDiff/2

        if abs(M - (weight * g * maxBeam)) < momConv:
            return fx, fy


def flap(hydHead, clVar=-0.1, highLim=0.1, lowLim=0.6):
    """
    Flap modeling. Implemented only a lift reduction
                                    | highLim
    0  _ _ _ _ _ _ _ _ _ _ _ _ _ _  _____________
                                  /
                               /
    clVar _________________ /
                   lowLim |
    :param hydHead: Hydrostatic head over the foil
    :param clVar: Maximum variation of the lift coefficient
    :param highLim: Minimum hydrostatic head beyond which the flap is maxed out at clVar
    :param lowLim: Maximum hydrostatic head beyond which the flap does not intervene

    :return clFlap: The variation in CL due to the flap
    """
    if hydHead < highLim:
        clFlap = clVar
    elif hydHead > lowLim:
        clFlap = 0
    else:
        clFlap = (hydHead - highLim)/(lowLim - highLim) * abs(clVar) + clVar
    return clFlap


def weight_movement(hydHead, vertV, pitch, timestep, angVel, minHead=0.8):
    """
    Pitch variation due to sailor weight movement model
    :param hydHead: Hydrostatic head over the foil [m]
    :param vertV: Vertical speed [m/s]
    :param pitch: Angle of pitch [radians]
    :param timestep: Value of the time discretization [s]
    :param angVel: Max angular speed possible [radians/s]
    :param minHead: Min hydrostatic head to start correcting pitch [m]

    :return pitch: The new value of pitch [radians]
    """
    AoAVar = angVel * timestep
    if hydHead < minHead:
        if vertV > 0.01:
            pitch -= AoAVar
        elif vertV < -0.01:
            pitch += AoAVar
    return pitch


def foil(speed, AoA, hydHead, CL, CD, CLred, Vels, Angs, strutTh=0.01, meanChord=0.1, meanTh=0.01, foilAR=10.5,
         rhoWater=1025, foilSurf=0.09):
    """
    :param speed: Foil Speed
    :param AoA: Angle of attack of the foil
    :param hydHead: Hydrostatic head over the foil
    :param CL: Matrix of the lift coefficients
    :param CD: Matrix of the drag coefficients
    :param CLred: Matrix of the reduction coefficients of CL due to free surface effects (CL/CLinf)
    :param Vels: Array of speeds (columns of CL and CD)
    :param Angs: Array of angles (rows of CL and CD)
    :param strutTh: Thickness of the strut
    :param meanChord: Mean chord of the main foil
    :param meanTh: Mean thickness of the main foil
    :param foilAR: Main foil aspect ratio span^2 / foil surface
    :param rhoWater: Density of water
    :param foilSurf: Foil surface

    :return 0-Lift: Lift force [N]
    :return 1-Drag: Drag force [N]
    """
    # Creating the response surfaces for interpolating CL and CD
    clSurface = interp2d(x=Vels, y=Angs, z=CL, kind='linear')
    cdSurface = interp2d(x=Vels, y=Angs, z=CD, kind='linear')
    # Creating the response surface for the reduction in CL due to free surface
    fnHArray = np.arange(0, 16, 0.1)  # TODO: Bring it outside the definition
    hcArray = np.arange(0.5, 5, 0.2)
    clRedSurface = interp2d(x=hcArray, y=fnHArray, z=CLred, kind='linear')
    # Computing the reduction in lift
    FnH = speed / sqrt(9.81 * abs(hydHead))
    hc = hydHead / meanChord
    if hc > 6:
        clReduction = 1
    else:
        clReduction = clRedSurface(hc, FnH)

    cl2D = clSurface(speed, AoA) * clReduction  # + flap(hydHead=hydHead)
    cd2D = cdSurface(speed, AoA)
    # Formulation 1 on (5) [page 9]
    cl3D = cl2D / (1 + 2 / foilAR)
    # Formulation 2 on (5) [page 10]
    cdI = cl3D**2 / (pi * foilAR)
    # Formulation 3 on (5) [page 10] with C_D / C_{Lh}^2 = 0.025
    # Attention errors in (5) c/h not h/c and C_D / C_{Lh}^2 = 0.025 and not 0.25 See formulation on page 9 on (7)
    cdW = cl3D**2 * meanChord / hydHead * 0.025
    # Formulation 6 on (5) [page 10]
    Dw = 0.5 * rhoWater * speed**2 * foilSurf * cdW
    # Formulation 4 on (5) [page 10] - with C_{DWS} = 0.54
    Dws = 0.5 * rhoWater * speed**2 * 0.54 * strutTh**2
    # Formulation 5 on (5) [page 10]
    Dj = 0.5 * rhoWater * speed**2 * (17 * (meanTh / meanChord)**2 - 0.05) * strutTh**2
    # Drag due to strut - 0.115 strut chord, 0.006 Naca 0012 CD from Xfoil
    Ds = 0.5 * rhoWater * speed**2 * (0.115 * hydHead) * 0.007

    Lift = 0.5 * rhoWater * speed**2 * foilSurf * cl3D
    Df = 0.5 * rhoWater * speed**2 * foilSurf * cd2D
    Di = 0.5 * rhoWater * speed**2 * foilSurf * cdI
    Drag = Df + Di + Dw + Dj + Dws + Ds
    return Lift[0], Drag[0]  # To obtain single value and not array


def start_angle(liftTakeOff, CL, Vels, Angs, vTakeoff=3.5, foilSurf=0.1, foilAR=10.5, rhoWater=1025, clConv=1e-4):
    """
    :param liftTakeOff: Lift to be generated at take off [N]
    :param CL: Matrix of the lift coefficients
    :param Vels: Array of speeds (columns of CL)
    :param Angs: Array of angles (columns of CL)
    :param vTakeoff: Speed at take off (must be in velX)
    :param foilSurf: Main foil planform area
    :param foilAR: Main foil aspect ratio span^2 / planform area
    :param rhoWater: Density of water
    :param clConv: Maximun difference in the lift coefficient if CLTO.max() is too low
    :return:
    """
    if vTakeoff in Vels:
        index = np.where(Vels == vTakeoff)[0]
    else:
        print('Error in the take off speed: not in speed list')
        return None, None
    CLTO = CL[:, index[0]]
    cl3D = liftTakeOff / (0.5 * rhoWater * vTakeoff**2 * foilSurf)
    cl2Obtain = cl3D * (1 + 2 / foilAR)
    if cl2Obtain > CLTO.max():
        foilSurf = liftTakeOff / (0.5 * rhoWater * vTakeoff**2 * CL[:, index].max() / (1 + 2 / foilAR))
        print('Cl 2D too low, new foil surface area: {:.4f}'.format(foilSurf))
        angIndex = np.where(CLTO == CLTO.max())
        foilAngle = Angs[angIndex][0]
    else:
        clTakeOff = interp1d(x=Angs, y=CLTO, kind='linear')
        angleIndex = np.where(CLTO > cl2Obtain)[0][0]
        angle1 = Angs[angleIndex]
        angle2 = angle1 - 0.5
        cl2 = clTakeOff(angle2)
        while abs(cl2 - cl2Obtain) > clConv:
            buff = angle2.copy()
            if cl2 - cl2Obtain < 0:
                angle2 = angle2 + abs(angle2 - angle1)/2
            else:
                angle2 = angle2 - abs(angle2 - angle1)/2
            angle1 = buff
            cl2 = clTakeOff(angle2)

        foilAngle = angle2
    return foilAngle, foilSurf


def dvpp_free_takeoff(fnameCL, fnameCD, fnameCLRed, Vels, Angs, m=100, mDistr=3/4, startingV=3.5, startingHead=0.99,
                      startingAngle=6, startingPitch=0, vWind=7, windAngle=pi/2, totalTime=6, timestep=1e-3,
                      angVel=1, foilSurf=0.09, flapLim=True, path=None):
    g = 9.81
    dragWithRudder = 1.75
    CL, CD, CLred = read_coeff_matrix(fnameCL=fnameCL, fnameCD=fnameCD, fnameCLred=fnameCLRed, path=path)
    mFoil = m * mDistr
    # AoAS, foilSurf = start_angle(mFoil * g, CL, Vels, Angs)  # TODO modify Starting angle
    # AoAS -= 0.037  # TODO
    AoAS = radians(startingAngle)

    # Starting data
    dist = 0
    velX = startingV
    accX = 0
    hydHead = startingHead
    velZ = 0
    accZ = 0
    pitch = radians(startingPitch)

    # Results array
    results = []

    for t in range(int(totalTime/timestep)):
        # moto rettilineo uniformemente accelerato
        dist = dist + velX * timestep + 0.5 * accX * timestep ** 2
        hydHead = hydHead - velZ * timestep - 0.5 * accZ * timestep ** 2
        pitch = weight_movement(hydHead=hydHead, vertV=velZ, pitch=pitch, timestep=timestep, angVel=radians(angVel))
        AoAreduction = np.arccos(velX / np.sqrt(velX ** 2 + velZ ** 2)) * copysign(1, velZ)
        AoA = AoAS - AoAreduction + pitch
        windAngleApp = np.arctan((vWind * np.sin(windAngle)) / (velX + vWind * np.cos(windAngle)))
        vWindApp = vWind * np.sin(windAngle) / np.sin(windAngleApp)
        fx_sail, fy_sail = sail_reduction(vWindApp, windAngleApp, hydHead=hydHead)
        Lift_foil, Drag_foil = foil(velX, AoA, hydHead, CL, CD, CLred, Vels, Angs, foilSurf=foilSurf)
        Drag_foil = Drag_foil * dragWithRudder
        accX = (fy_sail - Drag_foil) / mFoil
        accZ = (Lift_foil - mFoil * g) / mFoil
        velX = velX + timestep * accX
        velZ = velZ + timestep * accZ

        resultsTstep = [dist, velX, accX, hydHead, velZ, accZ, pitch, AoAreduction, AoA, vWindApp, windAngleApp,
                        fy_sail, Lift_foil, Drag_foil]
        results.append(resultsTstep)

        if AoA < np.radians(-2):
            print('error')

        # Print seconds finished
        if t*timestep in range(1, totalTime):
            print('Finished {} seconds of {}'.format(t*timestep, totalTime))

        # For debug at a certain timestep
        if t == 1950:
            debug = 0

        # Check for
        if hydHead < 0.01:  # TODO: limits inside definition
            print(f'out of water at time:{t*timestep}')
            return results
        if hydHead > 1.1:
            print(f'touched water at time:{t*timestep}')
            return results
    return results


def dvpp(fnameCL, fnameCD, fnameCLRed, startingV=3.5, path=None):
    CL, CD, CLred = read_coeff_matrix(fnameCL=fnameCL, fnameCD=fnameCD, fnameCLred=fnameCLRed, path=path)
    velList = np.asarray([(1.3*i)+2.2 for i in range(10)])
    angleList = np.asarray([(i-2) for i in range(11)])/180*pi
    betaReal = np.pi/2  # rad
    velW = 7  # m/s
    timestep = 0.001
    m = 100  # massa totale
    m_foil = m*(3/4)  # kgf a carico del foil principale
    t = [0]
    Vels = [3.5]
    Velv = [0]  # velocità verticale
    dist = [0]  # distanza x percorsa
    hydHead = [0.9]
    AoA, foilSurf = start_angle(m_foil*9.81, CL, velList, angleList)
    AoA -= 0.037
    AoAapp = [AoA]
    accX = [0]
    accZ = [0]
    convergenza = 0
    interval = [-0.1, 0.1]  # intervallo per la convergenza
    while convergenza == 0 and t[-1] < 6:
        # moto rettilineo uniformemente accelerato
        dist_step = dist[-1] + Vels[-1]*timestep + 0.5*accX[-1]*timestep**2
        dist.append(dist_step)
        hydHead_step = hydHead[-1] - Velv[-1]*timestep - 0.5*accZ[-1]*timestep**2
        hydHead.append(hydHead_step)
        Vels.append(Vels[-1] + timestep * accX[-1])
        Velv.append(Velv[-1] + timestep * accZ[-1])
        # Variazione di AoA dovuta allo spostamento di peso
        AoAW = weight_movement(hydHead=hydHead_step, pitch=0, vertV=Velv[-1], timestep=timestep, angVel=0.02)  # TODO
        # AoA, commentare la terza riga per avere la variazione d'angolo apparente, solo le prime due per AoA costante
        AoAreduction = np.arccos(Vels[-1]/np.sqrt(Vels[-1]**2+Velv[-1]**2)) * copysign(1, Velv[-1])
        AoAapp.append(AoA-AoAreduction+AoAW)
        # AoAapp.append(AoA)
        # altri calcoli
        betaApp = np.arctan((velW*np.sin(betaReal))/(Vels[-1]+velW*np.cos(betaReal)))
        vApp = velW*np.sin(betaReal)/np.sin(betaApp)
        fx_sail, fy_sail = sail(vApp, betaApp)
        Lift_foil, Drag_foil = foil(Vels[-1], AoAapp[-1], hydHead[-1], CL, CD, CLred, velList, angleList, foilSurf=foilSurf)
        accX.append((fy_sail - Drag_foil) / m_foil)
        accZ.append((Lift_foil-m_foil*9.81) / m_foil)
        if accX[-1] in interval and accZ[-1] in interval:
            convergenza = 1
        t.append(t[-1] + timestep)
        if len(t) == 1950:
            print('ok')
        if hydHead[-1] < 0.01:
            print(f'out of water at time:{len(t)}')
            return t, AoAapp, accX, accZ, Vels, Velv, dist, hydHead
        if hydHead[-1] > 1.1:
            print(f'touched water at time:{len(t)}')
            return t, AoAapp, accX, accZ, Vels, Velv, dist, hydHead
    return t, AoAapp, accX, accZ, Vels, Velv, dist, hydHead


if __name__ == '__main__':
    clFname = 'C:\\morphing_foil_codes\\morphing_foil\\Testing_inputs\\cl2350.npy'
    cdFname = 'C:\\morphing_foil_codes\\morphing_foil\\Testing_inputs\\cd2350.npy'
    clRedFname = 'C:\\morphing_foil_codes\\morphing_foil\\Testing_inputs\\clReduction.npy'
    velList = np.asarray([(1.3*i)+2.2 for i in range(10)])
    angleList = np.radians(np.asarray([(i-2) for i in range(11)]))
    tstep = 1e-3

    test = 2

    if test == 1:

        # Test 1
        time, AoAapparent, Ax, Az, Vx, Vz, x, h = dvpp(clFname, cdFname, clRedFname)
        AoAapparent = np.asarray(AoAapparent)*180/np.pi
        result = np.asarray([time, AoAapparent, Ax, Az, Vx, Vz, x, h]).T

        toPlotNames = ['AoAapparent', 'Ax', 'Az', 'velX', 'Vz', 'x', 'h']
        fig1 = plt.figure(figsize=(16, 9))
        for i in range(len(toPlotNames)):
            ax = fig1.add_subplot(len(toPlotNames), 1, i+1)
            ax.plot(time, result[:, i+1])
            # ax.set(title=toPlotNames[i])
            ax.text(0.5, 0.5, toPlotNames[i], transform=ax.transAxes, horizontalalignment='center')
            ax.grid(True)
        fig1.tight_layout()
        fig1.show()

    elif test == 2:
        res = np.asarray(dvpp_free_takeoff(fnameCL=clFname, fnameCD=cdFname, fnameCLRed=clRedFname, Vels=velList,
                                           Angs=angleList, m=100, mDistr=3/4, startingV=4, startingHead=0.9,
                                           startingAngle=5, vWind=5, windAngle=pi/2, totalTime=15, timestep=tstep,
                                           angVel=0.5, path=None))
        res[:, [6, 7, 8, 10]] = np.degrees(res[:, [6, 7, 8, 10]])

        resToPlot1 = {'Distance': 0,
                      'X speed': 1,
                      'X acceleration': 2,
                      'Hydrodinamic Head': 3,
                      'Z speed': 4,
                      'Z acceleration': 5}

        resToPlot2 = {'AoA variation by pitch': 6,
                      'AoA reduction by Z': 7,
                      'AoA Apparent': 8,
                      'Apparent wind speed': 9,
                      'Apparent wind angle': 10}
        resToPlot3 = {'Sail force on X': 11,
                      'Lift': 12,
                      'Drag': 13}

        fig2 = plt.figure(figsize=(16, 9))
        for i, key in enumerate(resToPlot1):
            ax = fig2.add_subplot(len(resToPlot1), 1, i+1)
            ax.plot(np.arange(len(res))*tstep, res[:, resToPlot1[key]])
            ax.text(0.5, 0.5, key, transform=ax.transAxes, horizontalalignment='center')
            ax.set_xlim(left=0, right=len(res)*tstep)
            ax.grid(True)
        fig2.tight_layout()
        fig2.show()

        fig3 = plt.figure(figsize=(16, 9))
        for i, key in enumerate(resToPlot2):
            ax = fig3.add_subplot(len(resToPlot2), 1, i+1)
            ax.plot(np.arange(len(res))*tstep, res[:, resToPlot2[key]])
            ax.text(0.5, 0.5, key, transform=ax.transAxes, horizontalalignment='center')
            ax.set_xlim(left=0, right=len(res)*tstep)
            ax.grid(True)
        fig3.tight_layout()
        fig3.show()

        fig4 = plt.figure(figsize=(16, 9))
        for i, key in enumerate(resToPlot3):
            ax = fig4.add_subplot(len(resToPlot3), 1, i+1)
            ax.plot(np.arange(len(res))*tstep, res[:, resToPlot3[key]])
            ax.text(0.5, 0.5, key, transform=ax.transAxes, horizontalalignment='center')
            ax.set_xlim(left=0, right=len(res)*tstep)
            ax.grid(True)
        fig4.tight_layout()
        fig4.show()

    # liftc, dragc, redc = read_coeff_matrix(fnameCL=clFname, fnameCD=cdFname, fnameCLred=clRedFname)
    # test1 = foil(6, radians(3), 0.5, liftc, dragc, redc, velList, angleList)
    #
    # for i in range(100):  # [0.4572*100-1]:
    #     test = foil(6.096, radians(3.4), (i+1)/100, liftc, dragc, redc, velList, angleList)
    #     print('Height {:} - Drag: {:5f} - Lift: {:5f}'.format((i+1)/100, test[1], test[0]))
