import pandas as pd

# Input data
file = 'C:\\Users\\richi\\Desktop\\test_run\\ModeFrontier\\FoilInput.txt'

pts = pd.read_csv(file, delimiter='\s+', skiprows=1, header=None, names=['x', 'y'])
pts['c'] = 1

le_idx = pts['x'].idxmin()
up = pts[['c', 'x', 'y']][:le_idx]
down = pts[['c', 'x', 'y']][le_idx:]

up.to_csv('C:\\Users\\richi\\Desktop\\up.txt', sep=' ', header=['Polyline', '=', 'True'], index=False)
down.to_csv('C:\\Users\\richi\\Desktop\\down.txt', sep=' ', header=['Polyline', '=', 'True'], index=False)

