import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

fMech = r'C:\Users\richi\Desktop\Confronto fsi mf\setup\Mechanical_2\Mechanical.dat'
fMF = r'C:\Users\richi\Desktop\Confronto fsi mf\Morphing_2d_out_UP_00006\proc\00000-00999\00000\Sub_1_00000\Mechanical_00004'
fMF2 = r'C:\Users\richi\Desktop\Confronto fsi mf\Morphing_2d_out_UP_00006\proc\00000-00999\00000\Sub_1_00000\Mechanical_00003'
fMFLD = r'C:\Users\richi\Desktop\Confronto fsi mf'
fFSI = r'C:\Users\richi\Desktop\Confronto fsi mf\export.csv'
fFSILD = r'C:\Users\richi\Desktop\Confronto fsi mf\FSI_LD.csv'

mechMesh = pd.read_csv(fMech, sep='\s+', header=None, names=['x', 'y', 'z'], index_col=0, skiprows=31, nrows=24067)
fsiId = pd.read_csv(fMech, sep='\s+', header=None, index_col=None, skiprows=38290, nrows=412).to_numpy()
fsiId = fsiId.reshape([fsiId.shape[0]*fsiId.shape[1], 1])
fsiId = fsiId[np.logical_not(np.isnan(fsiId))]
fsiId = fsiId.astype(int)

surfaceMesh = mechMesh.loc[fsiId]
plotMesh = surfaceMesh[surfaceMesh.z == 0]

x_data = pd.read_csv(fMF+r'\Out_x.dat', sep='\s+', names=['x'], skiprows=1)
y_data = pd.read_csv(fMF+r'\Out_y.dat', sep='\s+', names=['y'], skiprows=1)
z_data = pd.read_csv(fMF+r'\Out_z.dat', sep='\s+', names=['z'], skiprows=1)
data = x_data.copy()
data['y'] = y_data.copy()

x_data = pd.read_csv(fMF2+r'\Out_x.dat', sep='\s+', names=['x'], skiprows=1)
y_data = pd.read_csv(fMF2+r'\Out_y.dat', sep='\s+', names=['y'], skiprows=1)
z_data = pd.read_csv(fMF2+r'\Out_z.dat', sep='\s+', names=['z'], skiprows=1)
data2 = x_data.copy()
data2['y'] = y_data.copy()

x_data = pd.read_csv(fMFLD+r'\Large_def_mf_x.dat', sep='\s+', names=['x'], skiprows=1)
y_data = pd.read_csv(fMFLD+r'\Large_def_mf_y.dat', sep='\s+', names=['y'], skiprows=1)
dataLD = x_data.copy()
dataLD['y'] = y_data.copy()

defFsi = pd.read_csv(fFSI, header=None, names=['x', 'y', 'z', 'x0', 'y0'], index_col=None, skiprows=6)
defFsi = defFsi[defFsi.x > 62]

defFsiLD = pd.read_csv(fFSILD, header=None, names=['x', 'y', 'z'], index_col=None, skiprows=6)*1000
defFsiLD = defFsiLD[defFsiLD.x > 62]

fig = plt.figure()
ax = fig.add_subplot(111)
s_pt = 2
ax.plot(plotMesh.x, plotMesh.y, '.', label='Starting foil')
ax.plot(data.x, data.y, '.', label='ModeFrontier')
# ax.plot(data2.x, data2.y, '.', label='ModeFrontier it-1')
ax.plot(defFsi.x, defFsi.y, '.', label='FSI')
ax.plot(dataLD.x, dataLD.y, '.', label='ModeFrontier LD')
ax.plot(defFsiLD.x, defFsiLD.y, '.', label='FSI LD')
ax.legend()

fig.show()
