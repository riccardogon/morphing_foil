import matplotlib.pyplot as plt
import pandas as pd
import os
import sys


def defineDir():
    pass


def selection(options, opt_type):
    print('Select {} to check for iterations'.format(opt_type))
    for i, option in enumerate(options):
        print('[{}]\t{}'.format(i, option))
    sel = input('Input selected:\t')
    try:
        sel = int(sel)
        out = options[sel]
    except ValueError:
        print('\nINPUT IS NOT A NUMBER OR A VALID NUMBER!\n')
        out = selection(options, opt_type)
    except IndexError:
        print('\nNUMBER NOT IN LIST!\n')
        out = selection(options, opt_type)
    return out


def prj_sel(prj_dir):
    prjs = []
    dir_list = os.listdir(prj_dir)
    for file in dir_list:
        if file[-4:] == '.prj':
            prjs.append(file)
    prj = selection(prjs, opt_type='project')
    return prj[:-4]


def run_sel(prj_dir, prj):
    runs = 0
    for file in os.listdir(prj_dir):
        if file[:-6] == prj:
            runs += 1
    run = selection(range(runs), 'run')
    return run


def totDesNumber(proc):
    directories = os.listdir(proc)
    directories.remove('scheduler')
    directories.sort()

    last_dir = os.listdir(os.path.join(proc, directories[-1]))
    last_dir.sort()
    
    first_dir = os.listdir(os.path.join(proc, directories[0]))
    first_dir.sort()

    first_design = first_dir[0]
    last_design = last_dir[-1]
    return int(last_design), int(first_design)


def SelDes(max_des, min_des):
    sel = input('Select design to evaluate: {} - {}: '.format(min_des, max_des))
    try:
        sel = int(sel)
        if (sel >= min_des) and (sel <= max_des):
            pass
        else:
            raise ValueError
        out = sel
    except ValueError:
        print('\nINPUT IS NOT A NUMBER OR A VALID NUMBER!\n')
        out = SelDes(max_des, min_des)

    return out


def calcIter(des_folder, vel):
    iterations = 0
    for directory in os.listdir(os.path.join(des_folder, 'Sub_{}_00000'.format(vel))):
        if directory[:6] == 'Xfoil_':
            iterations += 1
        else:
            pass
    return iterations


def printIters(des_folder, vel, iterations, design):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for i in range(iterations):
        filename = os.path.join(des_folder, 'Sub_{}_00000'.format(vel), 'Xfoil_{:05d}'.format(i), 'FoilXfoil.txt')
        Buffer = pd.read_csv(filename, sep='\s+', skiprows=1, header=None)
        ax.plot(Buffer[0], Buffer[1], '-', label='{}'.format(i))
    filename = os.path.join(des_folder, 'Sub_{}_00000'.format(vel), 'ReadRes_{:05d}'.format(iterations-1),
                            'FoilInput.txt')
    try:
        Buffer = pd.read_csv(filename, sep='\s+', skiprows=1, header=None)
        ax.plot(Buffer[0], Buffer[1], '-', label='Last')
        filename = os.path.join(des_folder, 'Sub_{}_00000'.format(vel), 'CL_CD_00000', 'Polar.dat')
        if pd.read_csv(filename, sep='\s+', skiprows=10).shape[0] != 2:
            plt.text(0.5, 0.7, 'ERROR IN CL_CD', size=20, rotation=0, transform=ax.transAxes, ha='center')
    except FileNotFoundError:
        print('\nCrashed design. No last iteration found for vel {:}\n'.format(vel))
        plt.text(0.5, 0.7, 'Crashed design', size=20, rotation=0, transform=ax.transAxes, ha='center')
    ax.legend()
    ax.axis('equal')
    ax.set_title('Design {} Vel {}'.format(design, vel))
    fig.show()


def single(prj_dir):
    prj = prj_sel(prj_dir)
    run = run_sel(prj_dir, prj)

    proc = os.path.join(prj_dir, prj + '_{:05d}'.format(run), 'proc')

    max_des, min_des = totDesNumber(proc)
    design = SelDes(max_des, min_des)

    des_subf = design//1000 * 1000
    des_folder = os.path.join(proc, '{:05d}-{:05d}'.format(des_subf, des_subf+999), '{:05d}'.format(design))

    vel = selection([1, 2], 'velocity')
    iters = calcIter(des_folder, vel)
    printIters(des_folder=des_folder, vel=vel, iterations=iters, design=design)


def continuous():
    parent_dir = input('Input directory: ')
    n_vel = input('Number of subprocesses: ')
    while True:
        proc = os.path.join(parent_dir, 'proc')
        max_des, min_des = totDesNumber(proc)
        design = SelDes(max_des, min_des)

        des_subf = design // 1000 * 1000
        des_folder = os.path.join(proc, '{:05d}-{:05d}'.format(des_subf, des_subf + 999), '{:05d}'.format(design))

        for i, vel in enumerate([n for n in range(1, int(n_vel))]):
            try:
                iters = calcIter(des_folder, vel)
                printIters(des_folder, vel, iters, design)
            except FileNotFoundError:
                print('\nCrashed design. No vel {:}\n'.format(vel))


if __name__ == '__main__':
    try:
        continuous()
    except KeyboardInterrupt:
        print('\nExiting\n')
        sys.exit()