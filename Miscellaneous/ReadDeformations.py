import matplotlib.pyplot as plt
import os
import pandas as pd


DesDir = r'D:\MF_tesi\2350'
os.chdir(DesDir)

AnglesOrder = [0, 1, 2, 25, 26, 5, 28, 29, 8, 9, 10]
SpeedOrder = [(i * 1.3) + 2.2 for i in range(10)]

for i, speed in enumerate(range(1, 11)):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    startFoilFnm = '{:05}\\gh_00000\\Foilinput.txt'.format(0, speed)
    startFoil = pd.read_csv(startFoilFnm, skiprows=1, sep=',', names=['x', 'y'])
    ax.plot(startFoil.x, startFoil.y, 'k', label='Starting Foil')

    for j, angle in enumerate(AnglesOrder):
        foilFnm = '{:05}\\Sub_{:}_00000\\HydroCoeff_00000\\Foilinput.txt'.format(angle, speed)
        foilPts = pd.read_csv(foilFnm, skiprows=1, sep=',', names=['x', 'y'])
        ax.plot(foilPts.x, foilPts.y, label=str(j-2))
    
    ax.axis('equal')
    ax.legend()
    ax.set_title('Speed {:2.1f} m/s'.format(SpeedOrder[i]))
